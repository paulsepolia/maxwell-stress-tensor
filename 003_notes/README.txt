
1. Here I present the Mathematica code that I developed
   in order to integrate analytically the Maxwell Stress Tensor.
   The physics behind that intergation is the analytic
   evaluation of the force which exerted to a spherical homogeneous
   particle by the arbitrary incident electromagnetic wave.
   The force formalism is given by the closed integral over the surface
   of the particle of the mathematic's object called Maxwell Stress Tensor.
   An analytic integration of Mawell Stress Tensor was missing. 
   I did manage to give it an analytic solution using the rich symbolic 
   abilities of Mathematica programming language.

2. Note on 2024/12/15:
   The 'gamma' file has various efforts to speed-up the Force computation.
   The last effort one (all in RAM) amybe gives back erroneous results.

   #=========================#
   #  Pavlos G. Galiatsatos  #
   #  2013/08/15,            #
   #  Oxford, UK             #
   #=========================#
